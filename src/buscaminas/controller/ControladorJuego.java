package buscaminas.controller;

import buscaminas.model.Tablero;

/**
 *
 * @author Juanjo Vega <juanjo.vega@gmail.com>
 */
public class ControladorJuego {

    public static final int CONTINUAR = 0;
    public static final int GANA = 1;
    public static final int PIERDE = 2;

    Tablero tablero;
    boolean juegoTerminado;

    public ControladorJuego() {
        tablero = new Tablero();
    }

    public boolean juegoTerminado() {
        return juegoTerminado;
    }

    public void nuevaPartida() {
        juegoTerminado = false;
        tablero.init();
    }

    public int jugarEn(int fila, int columna) {
        int status = CONTINUAR;

        if (tablero.hayMinaEn(fila, columna)) {
            status = PIERDE;
        } else {
            tablero.jugarEn(fila, columna);

            if (!tablero.quedanMinas()) {
                status = GANA;
            }
        }

        juegoTerminado = status != CONTINUAR;

        return status;
    }

    public int getCasilla(int fila, int columna) {
        return tablero.getCasilla(fila, columna);
    }

    public void intercambiarBandera(int fila, int columna) {
        tablero.intercambiarBandera(fila, columna);
    }

    public boolean hayBanderaEn(int i, int j) {
        return tablero.hayBanderaEn(i, j);
    }
}
