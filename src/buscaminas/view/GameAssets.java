package buscaminas.view;

import javax.swing.ImageIcon;

/**
 *
 * @author Juanjo Vega <juanjo.vega@gmail.com>
 */
public class GameAssets {

    public final static ImageIcon IMG_MINA = new ImageIcon("./src/assets/mine.png");
    public final static ImageIcon IMG_FLAG = new ImageIcon("./src/assets/flag.png");
    public final static ImageIcon IMG_CLOSED = new ImageIcon("./src/assets/closed.png");
    final static ImageIcon IMG_EMPTY = new ImageIcon("./src/assets/empty.png");
    final static ImageIcon IMG_1 = new ImageIcon("./src/assets/1.png");
    final static ImageIcon IMG_2 = new ImageIcon("./src/assets/2.png");
    final static ImageIcon IMG_3 = new ImageIcon("./src/assets/3.png");
    final static ImageIcon IMG_4 = new ImageIcon("./src/assets/4.png");
    final static ImageIcon IMG_5 = new ImageIcon("./src/assets/5.png");
    final static ImageIcon IMG_6 = new ImageIcon("./src/assets/6.png");
    final static ImageIcon IMG_7 = new ImageIcon("./src/assets/7.png");
    final static ImageIcon IMG_8 = new ImageIcon("./src/assets/8.png");
    final static ImageIcon[] CASILLA = {IMG_EMPTY, IMG_1, IMG_2, IMG_3, IMG_4, IMG_5, IMG_6, IMG_7, IMG_8};

    public static ImageIcon getCasillaIcon(int valor_casilla) {
        return CASILLA[valor_casilla];
    }
}
