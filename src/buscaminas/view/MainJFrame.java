package buscaminas.view;

import buscaminas.controller.ControladorJuego;
import buscaminas.model.Tablero;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;

/**
 *
 * @author Juanjo Vega <juanjo.vega@gmail.com>
 */
public class MainJFrame extends javax.swing.JFrame {

    // Matriz de botones
    JLabel[][] botones;
    ControladorJuego controladorJuego = new ControladorJuego();

    /**
     * Creates new form MainJFrame
     */
    public MainJFrame() {
        initComponents();

        jpTablero.setLayout(new java.awt.GridLayout(Tablero.ROWS, Tablero.COLS));

        botones = new JLabel[Tablero.ROWS][Tablero.COLS];
        for (int i = 0; i < botones.length; i++) {
            for (int j = 0; j < botones[i].length; j++) {
                JLabel casilla = new JLabel();  // Creamos el botón...
                jpTablero.add(casilla);   // ...y lo añadimos a la GUI

                // Ajustamos sus parámetros.
                casilla.setHorizontalAlignment(SwingConstants.CENTER);

                final int i_aux = i;
                final int j_aux = j;

                casilla.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        if (!controladorJuego.juegoTerminado()) {
                            if (e.getButton() == MouseEvent.BUTTON1) {
                                onButtonClicked(i_aux, j_aux);
                            } else if (e.getButton() == MouseEvent.BUTTON3) {
                                onRightClick(i_aux, j_aux);
                            }
                        } else {
                            jlStatusBar.setText("El juego ha terminado. Pulse el botón de nueva partida.");
                        }
                    }
                });

                botones[i][j] = casilla;  // Lo guardamos.
            }
        }

        actualizarTablero();

        // pack(); // Ajustar al tamaño de los elementos que contiene.
        setIconImage(GameAssets.IMG_MINA.getImage());
        setLocationRelativeTo(null);    // Centrar en la pantalla
    }

    void onButtonClicked(int fila, int columna) {
        int resultado = controladorJuego.jugarEn(fila, columna);
        actualizarTablero();

        switch (resultado) {
            case ControladorJuego.PIERDE:
                pierde();
                break;
            case ControladorJuego.GANA:
                gana();
        }
    }

    void onRightClick(int fila, int columna) {
        ponerBandera(fila, columna);
    }

    void ponerBandera(int fila, int columna) {
        controladorJuego.intercambiarBandera(fila, columna);
        actualizarTablero();
    }

    void actualizarTablero() {
        for (int i = 0; i < botones.length; i++) {
            for (int j = 0; j < botones[i].length; j++) {
                if (controladorJuego.hayBanderaEn(i, j)) {
                    botones[i][j].setIcon(GameAssets.IMG_FLAG);
                } else {
                    int casilla = controladorJuego.getCasilla(i, j);

                    if (casilla >= 0) {
                        botones[i][j].setIcon(GameAssets.getCasillaIcon(casilla));
                    } else {
                        botones[i][j].setIcon(GameAssets.IMG_CLOSED);
                    }
                }
            }
        }
    }

    void pierde() {
        revelarTablero();
    }

    void revelarTablero() {
        for (int i = 0; i < botones.length; i++) {
            for (int j = 0; j < botones[i].length; j++) {
                if (controladorJuego.hayBanderaEn(i, j)) {
                    botones[i][j].setIcon(GameAssets.IMG_FLAG);
                } else {
                    int casilla = controladorJuego.getCasilla(i, j);

                    if (casilla >= 0) {
                        botones[i][j].setIcon(GameAssets.getCasillaIcon(casilla));
                    } else {
                        botones[i][j].setIcon(casilla == Tablero.MINA ? GameAssets.IMG_MINA : GameAssets.IMG_CLOSED);
                    }
                }
            }
        }
    }

    void gana() {
        JOptionPane.showMessageDialog(this, "Has ganado!");
    }

    void nuevaPartida() {
        jlStatusBar.setText(" ");
        controladorJuego.nuevaPartida();
        actualizarTablero();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jpControles = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jpTablero = new javax.swing.JPanel();
        jlStatusBar = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Buscaminas");
        setPreferredSize(new java.awt.Dimension(300, 300));

        jButton1.setText("Nueva partida");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jpControles.add(jButton1);

        jButton2.setText("Ver puntuaciones");
        jpControles.add(jButton2);

        getContentPane().add(jpControles, java.awt.BorderLayout.NORTH);

        jpTablero.setLayout(new java.awt.FlowLayout(java.awt.FlowLayout.CENTER, 0, 0));
        getContentPane().add(jpTablero, java.awt.BorderLayout.CENTER);

        jlStatusBar.setText(" ");
        getContentPane().add(jlStatusBar, java.awt.BorderLayout.SOUTH);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        nuevaPartida();
    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainJFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jlStatusBar;
    private javax.swing.JPanel jpControles;
    private javax.swing.JPanel jpTablero;
    // End of variables declaration//GEN-END:variables
}
