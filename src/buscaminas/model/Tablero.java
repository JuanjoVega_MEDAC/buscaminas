package buscaminas.model;

import java.util.Random;

/**
 *
 * @author Juanjo Vega <juanjo.vega@gmail.com>
 */
public class Tablero {

    public static int ROWS = 8, COLS = 8;
    int[][] tablero = new int[ROWS][COLS];
    boolean[][] banderas = new boolean[ROWS][COLS];

    public final static int CERRADA = -1;
    public final static int MINA = -2;

    public final static int N_MINAS = 8;
    int casillasLibres;

    public Tablero() {
        init();
    }

    public final void init() {
        for (int i = 0; i < tablero.length; i++) {
            for (int j = 0; j < tablero[i].length; j++) {
                tablero[i][j] = CERRADA;
                banderas[i][j] = false;
            }
        }

        plantarMinas();
        casillasLibres = ROWS * COLS;
    }

    void plantarMinas() {
        Random r = new Random();
        int fila, columna;

        for (int i = 0; i < N_MINAS; i++) {
            do {
                fila = r.nextInt(ROWS);
                columna = r.nextInt(COLS);
            } while (tablero[fila][columna] == MINA);

            tablero[fila][columna] = MINA;
        }
    }

    public void jugarEn(int fila, int columna) {
        quitarBandera(fila, columna);

        tablero[fila][columna] = contarMinas(fila, columna);
        casillasLibres--;

        // Si la casilla está vacía, abrimos las de alrededor recursivamente.
        if (tablero[fila][columna] < 1) {
            for (int i = fila - 1; i <= fila + 1; i++) {
                for (int j = columna - 1; j <= columna + 1; j++) {
                    if (dentroDeRango(i, j) && casillaCerrada(i, j)) {
                        jugarEn(i, j);
                    }
                }
            }
        }
    }

    int contarMinas(int fila, int columna) {
        int minas = 0;

        for (int i = fila - 1; i <= fila + 1; i++) {
            for (int j = columna - 1; j <= columna + 1; j++) {
                if (dentroDeRango(i, j) && hayMinaEn(i, j)) {
                    minas++;
                }
            }
        }

        return minas;
    }

    boolean dentroDeRango(int fila, int columna) {
        return fila >= 0 && fila < tablero.length && columna >= 0 && columna < tablero[0].length;
    }

    public boolean hayBanderaEn(int fila, int columna) {
        return banderas[fila][columna];
    }

    public void intercambiarBandera(int fila, int columna) {
        if (casillaCerrada(fila, columna)) {
            banderas[fila][columna] = !banderas[fila][columna];
        }
    }

    void quitarBandera(int fila, int columna) {
        banderas[fila][columna] = false;
    }

    public boolean hayMinaEn(int fila, int columna) {
        return tablero[fila][columna] == MINA;
    }

    boolean casillaCerrada(int fila, int columna) {
        return tablero[fila][columna] == CERRADA || tablero[fila][columna] == MINA;
    }

    public int getCasilla(int fila, int columna) {
        return tablero[fila][columna];
    }

    /*
    public void mostrar() {
        for (int i = 0; i < tablero.length; i++) {
            for (int j = 0; j < tablero[i].length; j++) {
                System.out.print(getCasillaAsString(tablero[i][j]) + " ");
            }
            System.out.println("");
        }
        System.out.println("---------------------------------");
    }
     */
 /*
    public static String getCasillaAsString(int casilla) {
        String str = null;

        switch (casilla) {
            case CERRADA:
                str = " ";
                break;
            case MINA:
                str = "*";
                break;
            default:
                str = String.valueOf(casilla);
        }

        return str;
    }*/
    public boolean quedanMinas() {
        return casillasLibres > N_MINAS;
    }
}
